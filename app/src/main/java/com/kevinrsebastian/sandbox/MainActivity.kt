package com.kevinrsebastian.sandbox

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.kevinrsebastian.sandbox.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        var user = User("John", "Smith")
        binding.user = user

        Handler().postDelayed(
            {
                user.firstName = "James"
                user.lastName = "Davis"
            },
            2000)
    }
}
