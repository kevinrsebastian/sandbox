package com.kevinrsebastian.sandbox

import android.databinding.BaseObservable
import android.databinding.Bindable

class User constructor(firstName: String, lastName: String)
    : BaseObservable() {

    @get:Bindable
    var firstName: String = firstName
    set(value) {
        field = value
        notifyPropertyChanged(BR.firstName)
    }

    @get:Bindable
    var lastName: String = lastName
        set(value) {
            field = value
            notifyPropertyChanged(BR.lastName)
        }
}

