package com.kevinrsebastian.sandbox

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    // JUnit provides rules through a test class field or a getter method. However, Kotlin
    // annotations point to a property, which JUnit doesn't recognize.
    // @JvmField allows Kotlin properties to be compiled with Java annotations and modifiers.
    @JvmField
    @Rule
    var activityTestRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java)

    @Test
    fun show() {
        // Show the activity screen
        activityTestRule.launchActivity(Intent())

        onView(withText("Hello John!"))
            .check(matches(isDisplayed()))
    }
}